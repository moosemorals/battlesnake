# SPDX-License-Identifier: ISC

# Disable built in rules
MAKEFLAGS += --no-builtin-rules --no-builtin-variables

SOURCE  := src
TARGET  := target
RUNTIME := linux-x64
CONFIG  := Release

PROJECT := $(shell cat .project)
SOURCES := $(shell find $(SOURCE) -regextype egrep \( -path $(SOURCE)/obj -o -path $(SOURCE)/bin \) -prune -o -regex '.*(cs|css|cshtml|csproj|html|js|json|svg|png)$$' -print )
VERSION := $(shell tools/version.sh patch)

CSPROJ  := $(addsuffix .csproj, $(SOURCE)/$(PROJECT))
DLL     := $(addsuffix .dll, $(TARGET)/$(PROJECT))
REMOTE  := $(addsuffix @wepiu, $(PROJECT))
UNIT	:= $(addsuffix .service, $(PROJECT))

.PHONY: all
all: $(DLL)

.PHONY: clean
clean:
	-rm -rf $(TARGET) $(WASMLIB)
	dotnet clean $(CSPROJ)

.PHONY: dll
dll: $(DLL)
$(DLL): $(SOURCES)
	dotnet restore $(CSPROJ)\
		--runtime $(RUNTIME)
	dotnet publish $(CSPROJ) \
		--configuration $(CONFIG) \
		--no-restore \
		--no-self-contained \
		--nologo \
		--output $(TARGET) \
		--runtime $(RUNTIME)

.PHONY: install
install:
	ssh $(REMOTE) mkdir -p .config/systemd/user
	rsync --archive --delete deploy/$(UNIT) $(REMOTE):.config/systemd/user/
	ssh $(REMOTE) systemctl --user daemon-reload
	ssh $(REMOTE) systemctl --user enable --now $(UNIT)

.PHONY: deploy
deploy:  $(DLL) sync restart

.PHONY: sync
sync:
	rsync --archive --delete --verbose --info=progress2 $(TARGET)/ $(REMOTE):app

.PHONY: restart
restart:
	ssh $(REMOTE) systemctl --user restart $(PROJECT)

.PHONY: logs
logs:
	ssh $(REMOTE) journalctl --user --unit $(PROJECT) --follow
