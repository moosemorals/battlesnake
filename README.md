# A brutally simple snake for Battlesnakes

[BattleSnakes](https://play.battlesnake.com/) is an online coding game based
on the traditional 'Snake' computer game.

The game is turn based, and every snake must move every turn. If a snake
collides with a second snake, it dies. If it runs out of heath (doesn't eat
enough) it dies. If it runs into the edge of the board, it dies.

The winner is the last snake that didn't die.

Snakes are controlled programatically. The game server sends a `move` request to
player's http servers every tick, and the player's server responds with one of
four directions (Up, Down, Left, Right). Everyone's moves are resolved at the
same time, dead snakes are removed from the board, and life goes on.

## My server

This is a fairly standard ASP Core 6 server. There's the usual startup noise in
Program.cs, a `HomeController` to show a basic "we're up" page, and a
`SnakeController` that's a thin wrapper around
[SnakeEngine.Move](src/SnakeEngine.cs).

There are also some models that are paper thin implementaions of the
[API](https://docs.battlesnake.com/api) types. (Except [Point](src/Models/Point.cs)
does some of it's own stuff.

## Møøse Bite

I've tried to keep the code simple. It's a stateless design, meaning all
decisions about the next move are based on the [game state](src/Models/GameState.cs)
that comes from the server.

Based on a suggestion from [husband](https://www.shinjuspottery.com), the
basic plan is to stay small and out of the way. (This isn't traditional
snake, there's no prize for longest snake!)

The snake makes its choices deterministically, given the same
game state it should make the same choice.

## Algorithm

Each turn the snake needs to find the 'best' direction to move in, where
best is:
  - Not a wall, hazard, or snake (including ourselves)
      - Except we can move into the square that our tail occupies (because
        it won't occupy it next turn)
          - Unless we've just eaten, when the tail doesn't move for a turn
            (to make us longer)
   - Not food
      - Unless we're hungry (health below a threshold)
      - Or there's nowhere better to move
   - Not where a snake is going to be next turn (that is, one of the three
     directions round a snake's head that isn't the snakes body)
      - Unless there's nowhere better to move
   - Towards food (if we're hungry and not next to food)
   - Towards our own tail
      - Unless we're in the first three turns of the game where we're
        stacked up on ourselves and it gets complicated
   - Towards the center of the board (Hugging the edges makes it to easy
     for other snakes to trap us)
   - Towards the biggest space availible
