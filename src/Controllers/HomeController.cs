namespace uk.osric.snake;

using Microsoft.AspNetCore.Mvc;

[Route("")]
public class HomeController : Controller {

    [HttpGet("")]
    public IActionResult Index() => View();
}
