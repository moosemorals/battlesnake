// SPDX-License-Identifier: ISC

namespace uk.osric.snake.controllers;

using System.Diagnostics.CodeAnalysis;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using uk.osric.snake.models;

[ApiController]
[Route("")]
[IgnoreAntiforgeryToken]
public class SnakeController : ControllerBase {

    public SnakeController(ILogger<SnakeController> log, ISnake snake) {
        _log = log;
        _snake = snake;
    }

    private readonly ILogger<SnakeController> _log;
    private readonly ISnake _snake;

    [Consumes("application/json")]
    [Produces("application/json")]
    [HttpPost("{name}/move")]
    [SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Part of public API")]
    public IActionResult Move(string? name, MoveState state) {
        using (_log.BeginScope(state.Game.Id)) {
            string move = _snake.GetNextMove(state).ToString().ToLowerInvariant();
            _log.LogInformation("Returning move {Move}", move);
            return Ok(new { move });
        }
    }

    [Produces("application/json")]
    [HttpGet("{name}")]
    [SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Part of public API")]
    public IActionResult Root(string? name) {
        return Ok(new {
            apiVersion = "1",
            author = "moosemorals",
            color = "#aa88aa",
            head = "trans-rights-scarf",
            tail = "round-bum",
            version = "2"
        });
    }

    [Consumes("application/json")]
    [Produces("application/json")]
    [HttpPost("{name}/start")]
    [SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Part of public API")]
    public IActionResult Start(string? name, MoveState state) {
        _snake.StartGame(state.Game.Id);
        return Ok();
    }

    [Consumes("application/json")]
    [Produces("application/json")]
    [HttpPost("{name}/end")]
    [SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Part of public API")]
    public IActionResult Stop(string? name, MoveState state) {
        _snake.StopGame(state.Game.Id);
        return Ok();
    }
}
