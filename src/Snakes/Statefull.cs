// SPDX-License-Identifier: ISC

namespace uk.osric.snake.snakes;

using System;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using uk.osric.snake;
using uk.osric.snake.models;

public class Stateful : ISnake {

    public Stateful(ILogger<Stateful> log, IGameStorage gameStorage) {
        _log = log;
        _gameStorage = gameStorage;
    }

    private const int GoodMove = 30;
    private const int GreatMove = 50;
    private const int Hungry = 25;
    private const int OkMove = 10;
    private const int InvalidMove = int.MinValue;
    private static readonly IReadOnlySet<Heading> AllHeadings = new HashSet<Heading>() { Heading.Up, Heading.Left, Heading.Down, Heading.Right };
    private readonly IGameStorage _gameStorage;
    private readonly ILogger _log;

    public Heading GetNextMove(MoveState state) {
        _log.LogDebug("State: {State}", JsonConvert.SerializeObject(state));
        _log.LogInformation("Turn {Turn}, Health {Health}, Length {Length}", state.Turn.LeftPad(), state.You.Health.LeftPad(), state.You.Length.LeftPad());
        _gameStorage.Move(state);

        List<WeightedMove> moves = new();
        foreach (Heading h in AllHeadings) {
            int weight = WeightMove(state, state.You, h);
            if (weight != InvalidMove) {
                moves.Add(new(h, weight));
            }
        }

        switch (moves.Count) {
            case 0:
                _log.LogDebug("No available moves");
                return Heading.Right;

            case 1: {
                    Heading next = moves[0].Heading;
                    _log.LogDebug("Forced to move {Heading}", next);
                    return next;
                }
            default:
                moves = moves.OrderByDescending(m => m.Weight).ToList();
                _log.LogDebug("Picking best move from {Moves}", moves);
                return moves[0].Heading;
        }
    }

    public static (int, int, bool) EvaluateArea(MoveState state, Snake snake, Heading h) {
        // Do a search of the area (tree/graph) reachable from the snake.Head.Move(h)
        // Return it's size and the number of (other) snake heads it contains

        Stack<Point> stack = new();
        HashSet<Point> visited = new();
        HashSet<Point> heads = new();
        bool myTail = false;

        stack.Push(snake.Head.Move(h));
        while (stack.Any()) {
            Point current = stack.Pop();

            if (visited.Contains(current)) {
                continue;
            }

            visited.Add(current);

            foreach (Heading dir in AllHeadings) {

                Point next = current.Move(dir);

                if (next == snake.Body.Last()) {
                    myTail = true;
                }

                if (state.Board.Snakes.Where(s => s.Head != snake.Head).Any(s => s.Head == next)) {
                    heads.Add(next);
                }

                if (!IsOutOfBounds(state, next) && !IsOccupied(state, next)) {
                    stack.Push(next);
                }
            }
        }

        return (visited.Count, heads.Count, myTail);
    }

    public void StartGame(string id) {
        _log.LogDebug("SnakeEngine: Start: Id {GameId}", id);
        _gameStorage.StartGame(id);
    }

    public void StopGame(string id) {
        _log.LogDebug("SnakeEngine: Stop: Id {GameId}", id);
        _gameStorage.EndGame(id);
    }

    private static bool IsFood(MoveState state, Point p)
        => state.Board.Food.Contains(p);

    private static bool IsFoodDirection(MoveState state, Snake snake, Heading h)
        => state.Board.Food.Any(f => snake.Head.RoughHeading(f) == h);

    private static bool IsTail(Snake snake, Point p)
        => snake.Body.Last() == p;

    private static bool IsHungry(Snake snake) => snake.Health < Hungry;

    private static bool HasJustEaten(Snake snake) => snake.Health == 100;

    private static bool IsOccupied(MoveState state, Point p)
        => state.Board.Hazards.Contains(p)
            || state.Board.Snakes
                .SelectMany(s => HasJustEaten(s) ? s.Body : s.Body.SkipLast(1))
                .Contains(p);

    private static bool IsOutOfBounds(MoveState state, Point p)
        => (p.X < 0) || (p.X >= state.Board.Width) || (p.Y < 0) || (p.Y >= state.Board.Width);

    private static Snake? GetSnakeByHead(MoveState state, Point p)
        => state.Board.Snakes.SingleOrDefault(s => s.Head == p);

    private static bool IsTailHeading(Snake snake, Heading h)
        => snake.Head.RoughHeading(snake.Body.Last()) == h;

    private static bool IsTowardsCenter(MoveState state, Snake snake, Heading h)
        => snake.Head.RoughHeading(state.Board.GetCenter()) == h;

    private static IEnumerable<Point> GenerateAdjacent(MoveState state, Point p, IEnumerable<Point>? alsoOccupied = null)
        => AllHeadings.Select(h => p.Move(h))
            .Where(p => !IsOutOfBounds(state, p) && !IsOccupied(state, p) && !(alsoOccupied?.Contains(p) ?? false));

    private int WeightMove(MoveState state, Snake snake, Heading h) {
        Point next = state.You.Head.Move(h);

        if (IsOutOfBounds(state, next)) {
            _log.LogDebug("{Heading} is out of bounds", h);
            // No point looking out of bounds
            return InvalidMove;
        }

        IEnumerable<Snake> otherSnakes = state.Board.Snakes.Where(s => s.Head != snake.Head);

        int weight = 0;
        if (IsOccupied(state, next)) {
            bool occupied = true;

            // Tail only counts as occupied sometimes
            // Specifically, if it's past turn 3 (because we're stacked on top of ourselves in early turns)
            // And we've not just eaten (i.e., health is != 100)
            // then the tail wil have moved by the time we get there, and that's a good place to move
            if (IsTail(snake, next) && state.Turn >= 3 && !HasJustEaten(snake)) {
                _log.LogDebug("{Heading} Want to move onto my own tail", h);
                occupied = false;
                weight += IsHungry(snake) ? -OkMove : GreatMove;
            }

            // Avoid other snakes heads unless we can beat them (are longer than them)
            // in a head-to-head collision
            if (GetSnakeByHead(state, next) is Snake s && s.Head != snake.Head && s.Length < snake.Length) {
                _log.LogDebug("{Heading} Want to collide with {SnakeName}", h, s.Name);
                occupied = false;
                weight += GreatMove;
            }

            // At this point, we might as well bail if the target is occupied
            if (occupied) {
                _log.LogDebug("{Heading} is occupied", h);
                return InvalidMove;
            }
        }

        // Tend to steer away from biger snakes
        foreach (Snake s in otherSnakes.Where(s => s.Length >= snake.Length)) {
            if (GenerateAdjacent(state, s.Head).Contains(next)) {
                _log.LogDebug("{Heading} Avoiding {SnakeName}", h, s.Name);
                weight -= GreatMove;
            }
        }

        // Given a choice, move towards the middle of the board
        if (IsTowardsCenter(state, snake, h)) {
            _log.LogDebug("{Heading} Want to move towards center", h);
            weight += OkMove;
        }

        // Given a choice, move towards our own tail
        if (IsTailHeading(snake, h)) {
            _log.LogDebug("{Heading} Want to move towards my tail", h);
            weight += OkMove;
        }

        // Food is a little complex. If we're hungry (low on health) then we should
        // eat as soon as possible. If we're much shorter than our oppenents, then
        // eating might be a good idea.
        // Otherwise, broadly speaking, don't eat
        if (IsHungry(snake)) {
            if (IsFood(state, next)) {
                _log.LogDebug("{Heading} Want to eat food next to me", h);
                weight += GreatMove * ((Hungry - snake.Health) / 5);
            } else if (IsFoodDirection(state, snake, h)) {
                _log.LogDebug("{Heading} Want to move towards food", h);
                weight += GoodMove * ((Hungry - snake.Health) / 5); ;
            }
        } else if (otherSnakes.Any() && otherSnakes.Average(s => s.Length) > snake.Length * 1.5) {
            if (IsFood(state, next)) {
                _log.LogDebug("{Heading} Want to eat to get bigger", h);
                weight += GoodMove;
            } else if (IsFoodDirection(state, snake, h)) {
                _log.LogDebug("{Heading} Heading towards food to get bigger", h);
                weight += GoodMove;
            }
        } else if (IsFoodDirection(state, snake, h)) {
            _log.LogDebug("{Heading} Want to avoid food", h);
            weight -= OkMove;
        }

        // Look at the area we're moving into
        (int size, int heads, bool containsTail) = EvaluateArea(state, snake, h);
        _log.LogDebug("{Heading} has {Heads} snake(s) and is {Size} big", h, heads, size);

        // Avoid other snakes
        weight -= heads * GoodMove;

        // Move into the area that contains our tail,
        // unless we've just eaten
        if (containsTail) {
            _log.LogDebug("{Heading} Want to move into area with our tail", h);
            if (!HasJustEaten(snake)) {
                weight += GreatMove;
            } else {
                weight += OkMove;
            }
        }

        // Otherwise, try to move to an area we can fit in
        weight += size / 5;
        if (size >= snake.Length) {
            _log.LogDebug("{Heading} Want to move into a big area", h);
            weight += GoodMove;
        } else {
            _log.LogDebug("{Heading} Want to avoid a small area", h);
            weight -= GoodMove;
        }

        return weight;
    }

    private record struct WeightedMove(Heading Heading, int Weight);
}
