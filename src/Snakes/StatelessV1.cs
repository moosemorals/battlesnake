// SPDX-License-Identifier: ISC

namespace uk.osric.snake.snakes;

using System;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using uk.osric.snake;
using uk.osric.snake.models;

public class StatelessV1 : ISnake {

    public StatelessV1(ILogger<StatelessV1> log) {
        _log = log;
    }

    private const int Hungry = 19;
    private static readonly IReadOnlySet<Heading> AllHeadings = new HashSet<Heading>() { Heading.Up, Heading.Left, Heading.Down, Heading.Right };
    private readonly ILogger _log;

    public Heading GetNextMove(MoveState state) {
        _log.LogDebug("State: {State}", JsonConvert.SerializeObject(state));

        // Two phase plan.
        // Phase one, get a list of possible directions to move in

        IEnumerable<Move> moves = GetPossibleMoves(state);

        // Phase two, pick best direction

        Heading next = PickNextMove(state, moves);
        _log.LogDebug("Moving {Heading}", next);
        return next;
    }

    public void StartGame(string id) => _log.LogDebug("SnakeEngine: Start: Id {GameId}", id);

    public void StopGame(string id) => _log.LogDebug("SnakeEngine: Stop: Id {GameId}", id);

    private static int AvailibleSpace(MoveState state, Point from) {
        IEnumerable<Point> obstructions = state.Board.Hazards.Concat(state.Board.Snakes.SelectMany(s => s.Body));
        List<Point> visited = new();
        Queue<Point> queue = new();
        queue.Enqueue(from);

        while (queue.Count > 0) {
            Point next = queue.Dequeue();
            if (visited.Contains(next)) {
                continue;
            }

            if (!obstructions.Contains(next)) {
                visited.Add(next);
                foreach (Heading h in AllHeadings) {
                    Point nextNext = next.Move(h);
                    if (!IsOutOfBounds(state, nextNext)) {
                        queue.Enqueue(nextNext);
                    }
                }
            }
        }

        return visited.Count;
    }

    private static Point FindClosest(Point me, IEnumerable<Point> points) => points.MinBy(p => me.Distance(p));

    private static int FindMyTail(MoveState state, Point from) {
        Point tail = state.You.Body.Last();
        IEnumerable<Point> obstructions = GetObstructions(state);

        List<Point> visited = new();

        Queue<Point> queue = new();
        queue.Enqueue(from);
        while (queue.Count > 0) {
            Point next = queue.Dequeue();
            if (next == tail) {
                return 1;
            }

            if (visited.Contains(next)) {
                continue;
            }

            if (!obstructions.Contains(next)) {
                visited.Add(next);
                foreach (Heading h in AllHeadings) {
                    Point nextNext = next.Move(h);
                    if (!IsOutOfBounds(state, nextNext)) {
                        queue.Enqueue(nextNext);
                    }
                }
            }
        }

        return 0;
    }

    private static IEnumerable<Point> GetObstructions(MoveState state) {
        return state.Board.Hazards.Concat(state.Board.Snakes.SelectMany(s => s.Body))
            .Except(new Point[] { state.You.Body.Last() });
    }

    /// <summary>
    /// Get a list of headings that aren't obstructed.
    /// </summary>
    private static IEnumerable<Move> GetPossibleMoves(MoveState state) {
        IEnumerable<Point> obstructions = GetObstructions(state);

        List<Move> moves = new();

        Heading? foodDir = null;
        if (state.Board.Food.Any()) {
            Point food = FindClosest(state.You.Head, state.Board.Food);
            foodDir = state.You.Head.RoughHeading(food);
        }

        Heading centerDir = state.You.Head.RoughHeading(state.Board.GetCenter());

        foreach (Heading h in AllHeadings) {
            Point next = state.You.Head.Move(h);
            if (obstructions.Contains(next) || IsOutOfBounds(state, next)) {
                continue;
            }

            MoveFlags flags = foodDir != null && foodDir == h ? MoveFlags.FoodDirection : MoveFlags.None;

            if (h == centerDir) {
                flags |= MoveFlags.CenterDirection;
            }

            if (state.You.Body.Last() == next) {
                if (state.You.Health == 100 || state.Turn <= 3) {
                    // Tail doesn't vanish if we've just eaten
                    // so this space counts as occupied
                    continue;
                }

                flags |= MoveFlags.Tail;
            }

            // Sat on our tail for the first few moves, but we've
            // got plenty of space
            if (state.Turn >= 3 && FindMyTail(state, next) == 1) {
                flags |= MoveFlags.TailArea;
            }

            if (state.Board.Food.Contains(next)) {
                flags |= MoveFlags.Food;
            }

            foreach (Snake s in state.Board.Snakes) {
                if (s.Head == state.You.Head) {
                    continue;
                }

                if (GetPossibleMoves(state, s).Contains(next)) {
                    flags |= s.Body.Length >= state.You.Body.Length ? MoveFlags.BigHead : MoveFlags.SmallHead;
                }
            }

            if (state.Board.Snakes
                .Where(s => s.Head != state.You.Head)
                .SelectMany(s => GetPossibleMoves(state, s))
                .Contains(next)) {
                flags |= MoveFlags.BigHead;
            }

            moves.Add(new Move(h, flags));
        }

        return moves;
    }

    private static IEnumerable<Point> GetPossibleMoves(MoveState state, Snake snake) {
        return AllHeadings
            .Select(h => snake.Head.Move(h))
            .Where(p => !IsOutOfBounds(state, p))
            .Where(p => !GetObstructions(state).Contains(p));
    }

    private static bool IsOutOfBounds(MoveState state, Point p) => p.X < 0 || p.X >= state.Board.Width || p.Y < 0 || p.Y >= state.Board.Height;

    private Heading PickNextMove(MoveState state, IEnumerable<Move> moves) {
        bool Match(MoveFlags left, MoveFlags right) => (left & right) != 0;

        int GetWeight(MoveFlags flags, bool hungry) {
            int weight = 0;
            if (Match(flags, MoveFlags.BigHead)) {
                weight -= 5;
            } else if (Match(flags, MoveFlags.SmallHead)) {
                weight += 2;
            }

            if (Match(flags, MoveFlags.Food)) {
                weight += hungry ? 3 : -1;
            } else if (Match(flags, MoveFlags.FoodDirection) && hungry) {
                weight += 3;
            }

            if (Match(flags, MoveFlags.Tail)) {
                weight += 3;
            } else if (Match(flags, MoveFlags.TailArea)) {
                weight += 2;
            }

            if (Match(flags, MoveFlags.CenterDirection)) {
                weight += 1;
            }

            return weight;
        }

        bool hungry = state.You.Health <= Hungry;

        if (!moves.Any()) {
            _log.LogDebug("No legal moves");
            return Heading.Up;
        } else if (moves.Count() == 1) {
            _log.LogDebug("Taking only legal move");
            return moves.First().Heading;
        } else {
            // Caclucate weights
            Dictionary<Heading, int> weights = moves.ToDictionary(m => m.Heading, m => GetWeight(m.Flags, hungry));

            // Are all the weights the same?
            int w = weights.Values.First();
            if (weights.Values.All(v => v == w)) {
                _log.LogDebug("Weights are equal, checking areas");
                // find bigest space
                foreach (Heading h in weights.Keys) {
                    Point next = state.You.Head.Move(h);
                    weights[h] += AvailibleSpace(state, next);
                }
            }

            _log.LogDebug("Picking highest weighted option {Weighted}", weights);

            // Return highest weighed value
            return weights.OrderByDescending(kv => kv.Value).First().Key;
        }
    }

    private record struct Move(Heading Heading, MoveFlags Flags);

    [Flags]
    private enum MoveFlags {
        None = 0,
        Food = 1 << 1,
        FoodDirection = 1 << 2,
        BigHead = 1 << 3,
        TailArea = 1 << 4,
        Tail = 1 << 5,
        CenterDirection = 1 << 6,
        SmallHead = 1 << 7,
    }
}
