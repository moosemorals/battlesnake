// SPDX-License-Identifier: ISC

namespace uk.osric.snake.models;

public record Game(
    string Id,
    string Map,
    string Source,
    int Timeout,
    Ruleset Ruleset

    );
