﻿// SPDX-License-Identifier: ISC

namespace uk.osric.snake.models;

public record Ruleset(
    string Name,
    string Version,
    Settings Settings
    );

public record Settings(
    int FoodSpawnChance,
    int MinimumFood,
    int HazardDamgePerTurn
    );

public record Royale(
    int ShrinkEveryNTurns
    );

public record Squad(
    bool AllowBodyCollisions,
    bool SharedElimiation,
    bool SharedHealth,
    bool SharedLength
    );
