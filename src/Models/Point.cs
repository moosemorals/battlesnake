// SPDX-License-Identifier: ISC
namespace uk.osric.snake.models;

public record struct Point(int X, int Y);

public static class PointMethods {

    public static int Distance(this Point me, Point other) => Math.Abs(me.X - other.X) + Math.Abs(me.Y - other.Y);

    public static Heading GetHeading(this Point me, Point other) {
        if (me.X == other.X) {
            if (me.Y > other.Y) {
                return Heading.Up;
            } else if (me.Y < other.Y) {
                return Heading.Down;
            }
        } else if (me.Y == other.Y) {
            if (me.X > other.X) {
                return Heading.Right;
            } else if (me.X < other.X) {
                return Heading.Left;
            }
        }

        throw new ArgumentException("Points are not adjacent");
    }

    public static bool IsAdjacent(this Point me, Point other) {
        int deltaX = me.X - other.X;
        int deltaY = me.Y - other.Y;
        return (me.X == other.X && deltaY >= -1 && deltaY <= 1) || (me.Y == other.Y && deltaX >= -1 && deltaX <= 1);
    }

    public static Point Move(this Point me, Heading d) {
        return d switch {
            Heading.Up => me with { Y = me.Y + 1 },
            Heading.Down => me with { Y = me.Y - 1 },
            Heading.Left => me with { X = me.X - 1 },
            Heading.Right => me with { X = me.X + 1 },
            _ => throw new System.NotImplementedException(),
        };
    }

    public static Heading RoughHeading(this Point me, Point other) {
        Point translated = other.Subtract(me);

        if (translated.X >= translated.Y) {
            if (translated.X >= -translated.Y) {
                return Heading.Right;
            } else {
                return Heading.Down;
            }
        } else if (translated.X >= -translated.Y) {
            return Heading.Up;
        } else {
            return Heading.Left;
        }
    }

    public static Point Subtract(this Point me, Point other) => new(me.X - other.X, me.Y - other.Y);

    /**
     * Get the direction to move to reach other from me
     */
}
