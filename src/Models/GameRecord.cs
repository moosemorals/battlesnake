﻿// SPDX-License-Identifier: ISC

namespace uk.osric.snake.models;

public record GameRecord(
    string Id,
    List<MoveState> MoveStates,
    DateTimeOffset StartTime,
    DateTimeOffset? EndTime,
    Outcome? Outcome
    );

public enum Outcome {
    StillPlaying,
    Won,
    HitEdge,
    HitSelf,
    HitSnake,
    Hunger,
    Unknown,
}
