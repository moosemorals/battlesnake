// SPDX-License-Identifier: ISC

namespace uk.osric.snake.models;

public record MoveState(
    Game Game,
    int Turn,
    Board Board,
    Snake You
    );
