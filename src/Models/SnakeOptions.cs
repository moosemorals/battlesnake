﻿// SPDX-License-Identifier: ISC

namespace uk.osric.snake.models;

using System.ComponentModel.DataAnnotations;

public class SnakeOptions {
    public const string SectionName = "Snake";

    [Required]
    public string SaveFolder { get; set; } = default!;
}
