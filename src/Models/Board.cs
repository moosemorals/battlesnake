// SPDX-License-Identifier: ISC
// SPDX-License-Identifier: ISC

namespace uk.osric.snake.models;

public record Board(
    int Height,
    int Width,
    Point[] Food,
    Point[] Hazards,
    Snake[] Snakes
    );

public static class BoardMethods {

    public static Point GetCenter(this Board board) => new((board.Width - 1) / 2, (board.Height - 1) / 2);
}
