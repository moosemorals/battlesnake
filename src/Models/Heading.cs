// SPDX-License-Identifier: ISC
// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.snake.models;

public enum Heading {
    Up,
    Right,
    Down,
    Left
}

public static class HeadingMethods {

    public static Heading Reverse(this Heading current) {
        return current switch {
            Heading.Up => Heading.Down,
            Heading.Down => Heading.Up,
            Heading.Left => Heading.Right,
            Heading.Right => Heading.Left,
            _ => throw new ArgumentException("Unknown heading"),
        };
    }

    public static Heading TurnLeft(this Heading current) {
        return current switch {
            Heading.Up => Heading.Right,
            Heading.Right => Heading.Down,
            Heading.Down => Heading.Left,
            Heading.Left => Heading.Up,
            _ => throw new ArgumentException("Unknown heading"),
        };
    }

    public static Heading TurnRight(this Heading current) {
        return current switch {
            Heading.Up => Heading.Left,
            Heading.Right => Heading.Up,
            Heading.Down => Heading.Right,
            Heading.Left => Heading.Down,
            _ => throw new ArgumentException("Unknown heading"),
        };
    }
}
