// SPDX-License-Identifier: ISC

namespace uk.osric.snake.models;

public record Snake(
    string Id,
    string Name,
    int Health,
    Point[] Body,
    int? Latency,
    Point Head,
    int Length,
    string Shout,
    string Squad

    );
