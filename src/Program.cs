// SPDX-License-Identifier: ISC
namespace uk.osric.snake;

using System.Diagnostics;
using System.Globalization;

using Microsoft.AspNetCore.HttpLogging;

using uk.osric.snake.models;
using uk.osric.snake.snakes;
using uk.osric.snake.storage;

public class Program {

    public static void Main(string[] args) {
        WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

        builder.Host.UseSystemd();
        builder.WebHost.ConfigureLogging((context, logging) => {
            logging.ClearProviders();
            logging.AddConfiguration(context.Configuration.GetRequiredSection("Logging"));
            logging.AddConsole();
            if (builder.Environment.IsDevelopment()) {
                logging.AddDebug();
            }
        });

        builder.Services.AddOptions<SnakeOptions>()
            .Bind(builder.Configuration.GetRequiredSection(SnakeOptions.SectionName))
            .ValidateOnStart()
            .ValidateDataAnnotations();

        builder.Services.AddControllersWithViews()
            .AddNewtonsoftJson();

        builder.Services.AddHttpLogging(options
            => options.LoggingFields = HttpLoggingFields.RequestProperties | HttpLoggingFields.ResponseStatusCode);

        builder.Services.AddSingleton<IGameStorage, FileStorage>();
        builder.Services.AddSingleton<ISnake, Stateful>();

        WebApplication app = builder.Build();

        app.UseHttpLogging();

        app.Use((context, next) => {
            Stopwatch sw = Stopwatch.StartNew();
            Task result = next(context);
            sw.Stop();
            app.Logger.LogDebug("Time: {Duration} Path: {Path}", sw.ElapsedMilliseconds.LeftPad(4), context.Request.Path);
            return result;
        });

        app.MapControllers();

        app.Run();
    }
}
