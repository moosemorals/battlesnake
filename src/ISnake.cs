﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.snake;

using uk.osric.snake.models;

public interface ISnake {
    Heading GetNextMove(MoveState state);
    void StartGame(string id);
    void StopGame(string id);
}