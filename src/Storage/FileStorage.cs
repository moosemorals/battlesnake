﻿// SPDX-License-Identifier: ISC

namespace uk.osric.snake.storage;

using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using uk.osric.snake.models;

public class FileStorage : IGameStorage, IHostedService {

    public FileStorage(ILogger<FileStorage> log, IOptions<SnakeOptions> options) {
        _options = options.Value;
        _log = log;
    }

    private readonly object _fsLock = new();
    private readonly ILogger<FileStorage> _log;
    private readonly SnakeOptions _options;
    private readonly Queue<MoveState> _queue = new();
    private readonly CancellationTokenSource _shutdownSource = new();

    public Task EndGame(string id) {
        lock (_fsLock) {
            GameRecord record = LoadGame(id).GetAwaiter().GetResult();
            record = record with { EndTime = DateTimeOffset.Now, Outcome = Outcome.Unknown };
            SaveGameRecord(id, record);
        }

        return Task.CompletedTask;
    }

    public Task<GameRecord> LoadGame(string id) {
        string path = GetSaveFilePath(id);
        string json = File.ReadAllText(path, Encoding.UTF8);

        if (JsonConvert.DeserializeObject<GameRecord>(json) is GameRecord gameRecord) {
            return Task.FromResult(gameRecord);
        }

        throw new JsonException($"Could not parse '{path}' into GameRecord");
    }

    public Task Move(MoveState moveState) {
        Enqueue(moveState);
        return Task.CompletedTask;
    }

    public Task StartAsync(CancellationToken cancellationToken) {
        new Thread(Run).Start();
        return Task.CompletedTask;
    }

    public Task StartGame(string id) {
        GameRecord gameRecord = new(id, new List<MoveState>(), DateTimeOffset.Now, null, Outcome.StillPlaying);
        lock (_fsLock) {
            SaveGameRecord(id, gameRecord);
        }

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken) {
        _shutdownSource.Cancel();
        return Task.CompletedTask;
    }

    private void Enqueue(MoveState move) {
        lock (_queue) {
            _queue.Enqueue(move);
            Monitor.Pulse(_queue);
        }
    }

    private string GetSaveFilePath(string id)
        => Path.GetFullPath(Path.Join(_options.SaveFolder, $"{id}.json"));

    private void Run() {
        using (_log.BeginScope("Background thread")) {
            _log.LogDebug("Started");
            while (!_shutdownSource.IsCancellationRequested) {
                MoveState next;
                lock (_queue) {
                    while (_queue.Count == 0) {
                        Monitor.Wait(_queue);
                    }

                    next = _queue.Dequeue();
                }

                lock (_fsLock) {
                    string id = next.Game.Id;
                    GameRecord record = LoadGame(id).GetAwaiter().GetResult();
                    record.MoveStates.Add(next);
                    SaveGameRecord(id, record);
                }
            }

            _log.LogDebug("Ended");
        }
    }

    private void SaveGameRecord(string id, GameRecord gameRecord) {
        string savePath = GetSaveFilePath(id);
        _log.LogDebug("Saving GameRecord {GameId} to {SaveFilePath} (Base folder {SaveFolder}", id, savePath, _options.SaveFolder);
        Directory.CreateDirectory(_options.SaveFolder);
        string json = JsonConvert.SerializeObject(gameRecord);
        File.WriteAllText(savePath, json, Encoding.UTF8);
    }
}
