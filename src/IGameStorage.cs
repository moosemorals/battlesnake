﻿// SPDX-License-Identifier: ISC

namespace uk.osric.snake;

using uk.osric.snake.models;

public interface IGameStorage {

    public Task EndGame(string id);

    public Task<GameRecord> LoadGame(string id);

    public Task Move(MoveState moveState);

    public Task StartGame(string id);
}
