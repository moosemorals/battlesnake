﻿// SPDX-License-Identifier: ISC

namespace uk.osric.snake;

using System.Globalization;

public static class Extensions {

    // Based on a reply to https://stackoverflow.com/a/33336576
    public static IEnumerable<IEnumerable<T>> Combinations<T>(this IEnumerable<T> source, int k) {
        return k == 0
            ? Enumerable.Repeat(Enumerable.Empty<T>(), 1)
            : source.SelectMany((e, i) => source.Skip(i + 1)
                .Combinations(k - 1)
                .Select(c => c.Append(e)));
    }

    public static string LeftPad(this long v, int c = 3)
        => v.ToString("D", CultureInfo.InvariantCulture).PadLeft(c);

    public static string LeftPad(this int v, int c = 3)
        => v.ToString("D", CultureInfo.InvariantCulture).PadLeft(c);
}
