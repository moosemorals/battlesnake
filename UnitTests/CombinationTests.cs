// SPDX-License-Identifier: ISC

namespace uk.osric.snake.tests;

public class CombinationTests {

    [Test]
    public void SmokeTest() {
        List<int> numbers = new() { 1, 2, 3 };

        var actual = numbers.Combinations(2);

        Assert.That(actual, Is.Not.Null);
        Assert.That(actual.Count(), Is.EqualTo(3));
    }

    [Test]
    public void HandlesZero() {
        var actual = Array.Empty<int>().Combinations(2);

        Assert.That(actual, Is.Not.Null);
        Assert.That(actual, Is.Empty);
    }
}
