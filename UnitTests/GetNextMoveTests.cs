﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.snake.tests;

using Microsoft.Extensions.Logging.Abstractions;

using Newtonsoft.Json;

using uk.osric.snake.models;
using uk.osric.snake.snakes;

internal class GetNextMoveTests {

    [TestCaseSource(nameof(GetNextMoveCases))]
    public void GetNextMove(string json, Heading expected) {

        if (JsonConvert.DeserializeObject<MoveState>(json) is not MoveState state) {
            throw new JsonException("Can't deserialize state");
        }

        Stateful engine = new(new NullLogger<Stateful>(), new FakeGameStorage());

        Heading actual = engine.GetNextMove(state);

        Assert.That(actual, Is.EqualTo(expected));
    }


    [TestCaseSource(nameof(EvaluateAreaCases))]
    public void EvaluateAreaTest(string json, Heading h, int expectedSize, int expectedHeads, bool expectedTail) {
        if (JsonConvert.DeserializeObject<MoveState>(json) is not MoveState state) {
            throw new JsonException("Can't deserialize state");
        }

        (int actualSize, int actualHeads, bool actualTail) = Stateful.EvaluateArea(state, state.You, h);
        Assert.Multiple(() => {
            Assert.That(actualSize, Is.EqualTo(expectedSize));
            Assert.That(actualHeads, Is.EqualTo(expectedHeads));
            Assert.That(actualTail, Is.EqualTo(expectedTail));
        });
    }

    public static readonly object[] GetNextMoveCases = {
        new object[] { "{\"Game\":{\"Id\":\"0f1fce3b-0767-45a3-8332-90b52b082f79\",\"Map\":\"standard\",\"Source\":\"challenge\",\"Timeout\":500,\"Ruleset\":{\"Name\":\"solo\",\"Version\":\"v1.1.20\",\"Settings\":{\"FoodSpawnChance\":15,\"MinimumFood\":1,\"HazardDamgePerTurn\":0}}},\"Turn\":254,\"Board\":{\"Height\":7,\"Width\":7,\"Food\":[{\"X\":0,\"Y\":2},{\"X\":3,\"Y\":3},{\"X\":5,\"Y\":2},{\"X\":4,\"Y\":5},{\"X\":3,\"Y\":2},{\"X\":1,\"Y\":2},{\"X\":4,\"Y\":0},{\"X\":3,\"Y\":6},{\"X\":2,\"Y\":2},{\"X\":2,\"Y\":0},{\"X\":6,\"Y\":6},{\"X\":4,\"Y\":3},{\"X\":6,\"Y\":4},{\"X\":2,\"Y\":1},{\"X\":0,\"Y\":0},{\"X\":3,\"Y\":1},{\"X\":5,\"Y\":3},{\"X\":5,\"Y\":4},{\"X\":6,\"Y\":1},{\"X\":2,\"Y\":3},{\"X\":2,\"Y\":4},{\"X\":3,\"Y\":5},{\"X\":6,\"Y\":5},{\"X\":1,\"Y\":4},{\"X\":5,\"Y\":5},{\"X\":1,\"Y\":0},{\"X\":0,\"Y\":1},{\"X\":6,\"Y\":2},{\"X\":5,\"Y\":1},{\"X\":4,\"Y\":4}],\"Hazards\":[],\"Snakes\":[{\"Id\":\"gs_xxgR7XCwpfpxPxRMRMXGWVPf\",\"Name\":\"Møøse Bite\",\"Health\":18,\"Body\":[{\"X\":0,\"Y\":6},{\"X\":1,\"Y\":6},{\"X\":2,\"Y\":6},{\"X\":2,\"Y\":5},{\"X\":1,\"Y\":5},{\"X\":0,\"Y\":5}],\"Latency\":15,\"Head\":{\"X\":0,\"Y\":6},\"Length\":6,\"Shout\":\"\",\"Squad\":\"\"}]},\"You\":{\"Id\":\"gs_xxgR7XCwpfpxPxRMRMXGWVPf\",\"Name\":\"Møøse Bite\",\"Health\":18,\"Body\":[{\"X\":0,\"Y\":6},{\"X\":1,\"Y\":6},{\"X\":2,\"Y\":6},{\"X\":2,\"Y\":5},{\"X\":1,\"Y\":5},{\"X\":0,\"Y\":5}],\"Latency\":15,\"Head\":{\"X\":0,\"Y\":6},\"Length\":6,\"Shout\":\"\",\"Squad\":\"\"}}", Heading.Down },
        new object[] { "{\"Game\":{\"Id\":\"e69869b8-52d3-482c-a6b7-f553b851e50c\",\"Map\":\"standard\",\"Source\":\"challenge\",\"Timeout\":500,\"Ruleset\":{\"Name\":\"solo\",\"Version\":\"v1.1.20\",\"Settings\":{\"FoodSpawnChance\":15,\"MinimumFood\":1,\"HazardDamgePerTurn\":0}}},\"Turn\":368,\"Board\":{\"Height\":7,\"Width\":7,\"Food\":[{\"X\":0,\"Y\":0},{\"X\":5,\"Y\":0},{\"X\":5,\"Y\":1},{\"X\":1,\"Y\":0},{\"X\":2,\"Y\":0},{\"X\":2,\"Y\":1},{\"X\":6,\"Y\":2},{\"X\":6,\"Y\":0},{\"X\":1,\"Y\":2},{\"X\":0,\"Y\":3},{\"X\":0,\"Y\":1},{\"X\":6,\"Y\":1},{\"X\":0,\"Y\":2},{\"X\":1,\"Y\":1},{\"X\":2,\"Y\":2}],\"Hazards\":[],\"Snakes\":[{\"Id\":\"gs_qhpMf3T9XHTdG79Q344WHYrX\",\"Name\":\"Møøse Bite\",\"Health\":99,\"Body\":[{\"X\":5,\"Y\":3},{\"X\":6,\"Y\":3},{\"X\":6,\"Y\":4},{\"X\":5,\"Y\":4},{\"X\":5,\"Y\":5},{\"X\":6,\"Y\":5},{\"X\":6,\"Y\":6},{\"X\":5,\"Y\":6},{\"X\":4,\"Y\":6},{\"X\":4,\"Y\":5},{\"X\":3,\"Y\":5},{\"X\":3,\"Y\":6},{\"X\":2,\"Y\":6},{\"X\":2,\"Y\":5},{\"X\":1,\"Y\":5},{\"X\":1,\"Y\":6},{\"X\":0,\"Y\":6},{\"X\":0,\"Y\":5},{\"X\":0,\"Y\":4},{\"X\":1,\"Y\":4},{\"X\":1,\"Y\":3},{\"X\":2,\"Y\":3},{\"X\":2,\"Y\":4},{\"X\":3,\"Y\":4},{\"X\":4,\"Y\":4},{\"X\":4,\"Y\":3},{\"X\":3,\"Y\":3},{\"X\":3,\"Y\":2},{\"X\":3,\"Y\":1},{\"X\":3,\"Y\":0},{\"X\":4,\"Y\":0},{\"X\":4,\"Y\":1},{\"X\":4,\"Y\":2},{\"X\":5,\"Y\":2}],\"Latency\":22,\"Head\":{\"X\":5,\"Y\":3},\"Length\":34,\"Shout\":\"\",\"Squad\":\"\"}]},\"You\":{\"Id\":\"gs_qhpMf3T9XHTdG79Q344WHYrX\",\"Name\":\"Møøse Bite\",\"Health\":99,\"Body\":[{\"X\":5,\"Y\":3},{\"X\":6,\"Y\":3},{\"X\":6,\"Y\":4},{\"X\":5,\"Y\":4},{\"X\":5,\"Y\":5},{\"X\":6,\"Y\":5},{\"X\":6,\"Y\":6},{\"X\":5,\"Y\":6},{\"X\":4,\"Y\":6},{\"X\":4,\"Y\":5},{\"X\":3,\"Y\":5},{\"X\":3,\"Y\":6},{\"X\":2,\"Y\":6},{\"X\":2,\"Y\":5},{\"X\":1,\"Y\":5},{\"X\":1,\"Y\":6},{\"X\":0,\"Y\":6},{\"X\":0,\"Y\":5},{\"X\":0,\"Y\":4},{\"X\":1,\"Y\":4},{\"X\":1,\"Y\":3},{\"X\":2,\"Y\":3},{\"X\":2,\"Y\":4},{\"X\":3,\"Y\":4},{\"X\":4,\"Y\":4},{\"X\":4,\"Y\":3},{\"X\":3,\"Y\":3},{\"X\":3,\"Y\":2},{\"X\":3,\"Y\":1},{\"X\":3,\"Y\":0},{\"X\":4,\"Y\":0},{\"X\":4,\"Y\":1},{\"X\":4,\"Y\":2},{\"X\":5,\"Y\":2}],\"Latency\":22,\"Head\":{\"X\":5,\"Y\":3},\"Length\":34,\"Shout\":\"\",\"Squad\":\"\"}}", Heading.Down },
    };

    public static readonly object[] EvaluateAreaCases = {
        new object[] { "{\"Game\":{\"Id\":\"2d4b3aba-6d0e-4256-b544-a8b22cbeaa4a\",\"Map\":\"standard\",\"Source\":\"custom\",\"Timeout\":500,\"Ruleset\":{\"Name\":\"standard\",\"Version\":\"v1.1.20\",\"Settings\":{\"FoodSpawnChance\":15,\"MinimumFood\":1,\"HazardDamgePerTurn\":0}}},\"Turn\":19,\"Board\":{\"Height\":11,\"Width\":11,\"Food\":[{\"X\":5,\"Y\":5},{\"X\":8,\"Y\":5}],\"Hazards\":[],\"Snakes\":[{\"Id\":\"gs_T8g4gj3xCR8QJwYWGSfVxQxb\",\"Name\":\"Sigma Grindset\",\"Health\":95,\"Body\":[{\"X\":5,\"Y\":6},{\"X\":5,\"Y\":7},{\"X\":5,\"Y\":8},{\"X\":5,\"Y\":9},{\"X\":5,\"Y\":10}],\"Latency\":85,\"Head\":{\"X\":5,\"Y\":6},\"Length\":5,\"Shout\":\"\",\"Squad\":\"\"},{\"Id\":\"gs_BWRYYcC3DcrkKRK8qcjQ8BPS\",\"Name\":\"Golden\",\"Health\":95,\"Body\":[{\"X\":2,\"Y\":5},{\"X\":1,\"Y\":5},{\"X\":1,\"Y\":4},{\"X\":0,\"Y\":4},{\"X\":0,\"Y\":5}],\"Latency\":123,\"Head\":{\"X\":2,\"Y\":5},\"Length\":5,\"Shout\":\"\",\"Squad\":\"\"},{\"Id\":\"gs_d979x69SPp4fXrq34Q8FVwvX\",\"Name\":\"Møøse Bite\",\"Health\":81,\"Body\":[{\"X\":5,\"Y\":4},{\"X\":4,\"Y\":4},{\"X\":4,\"Y\":3}],\"Latency\":21,\"Head\":{\"X\":5,\"Y\":4},\"Length\":3,\"Shout\":\"\",\"Squad\":\"\"}]},\"You\":{\"Id\":\"gs_d979x69SPp4fXrq34Q8FVwvX\",\"Name\":\"Møøse Bite\",\"Health\":81,\"Body\":[{\"X\":5,\"Y\":4},{\"X\":4,\"Y\":4},{\"X\":4,\"Y\":3}],\"Latency\":21,\"Head\":{\"X\":5,\"Y\":4},\"Length\":3,\"Shout\":\"\",\"Squad\":\"\"}}", Heading.Left, 109, 2, true },
        new object[] { "{\"Game\":{\"Id\":\"178cba9c-da73-4bfd-9550-5c6d812daf61\",\"Map\":\"standard\",\"Source\":\"custom\",\"Timeout\":500,\"Ruleset\":{\"Name\":\"standard\",\"Version\":\"v1.1.20\",\"Settings\":{\"FoodSpawnChance\":15,\"MinimumFood\":1,\"HazardDamgePerTurn\":0}}},\"Turn\":18,\"Board\":{\"Height\":11,\"Width\":11,\"Food\":[{\"X\":0,\"Y\":8},{\"X\":0,\"Y\":2},{\"X\":7,\"Y\":8},{\"X\":6,\"Y\":10}],\"Hazards\":[],\"Snakes\":[{\"Id\":\"gs_mYYMcKQtS3KfB7k4rKySWgGR\",\"Name\":\"Møøse Bite\",\"Health\":82,\"Body\":[{\"X\":4,\"Y\":6},{\"X\":4,\"Y\":7},{\"X\":4,\"Y\":8}],\"Latency\":20,\"Head\":{\"X\":4,\"Y\":6},\"Length\":3,\"Shout\":\"\",\"Squad\":\"\"},{\"Id\":\"gs_kkcKXkJ4d46Hw4Cgdvt76B9H\",\"Name\":\"Sigma Grindset\",\"Health\":100,\"Body\":[{\"X\":5,\"Y\":5},{\"X\":5,\"Y\":4},{\"X\":4,\"Y\":4},{\"X\":3,\"Y\":4},{\"X\":2,\"Y\":4},{\"X\":2,\"Y\":4}],\"Latency\":86,\"Head\":{\"X\":5,\"Y\":5},\"Length\":6,\"Shout\":\"\",\"Squad\":\"\"}]},\"You\":{\"Id\":\"gs_mYYMcKQtS3KfB7k4rKySWgGR\",\"Name\":\"Møøse Bite\",\"Health\":82,\"Body\":[{\"X\":4,\"Y\":6},{\"X\":4,\"Y\":7},{\"X\":4,\"Y\":8}],\"Latency\":20,\"Head\":{\"X\":4,\"Y\":6},\"Length\":3,\"Shout\":\"\",\"Squad\":\"\"}}", Heading.Right, 113, 1, true }
    };
}
