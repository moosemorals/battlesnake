﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.snake.tests;
using System;
using System.Threading.Tasks;

using uk.osric.snake.models;

internal class FakeGameStorage : IGameStorage {
    public Task EndGame(string id) => Task.CompletedTask;
    public Task<GameRecord> LoadGame(string id) => throw new NotImplementedException();
    public Task Move(MoveState moveState) => Task.CompletedTask;
    public Task StartGame(string id) => Task.CompletedTask;
}
