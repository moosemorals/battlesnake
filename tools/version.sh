#!/bin/bash

V=$( cat .version.major | tr --delete "\r\n\t " )
MAJOR="${V:-1}"
V=$( cat .version.minor | tr --delete "\r\n\t " )
MINOR="${V:-0}"
V=$( cat .version.patch | tr --delete "\r\n\t " )
PATCH="${V:-0}"

case $1 in
  major)
    MAJOR=$(( MAJOR + 1 ))
    MINOR="0"
    PATCH="0"
    ;;
  minor)
    MINOR=$(( $MINOR + 1 ))
    PATCH="0"
    ;;
  patch)
    PATCH=$(( $PATCH + 1 ))
    ;;
  init)
    MAJOR="1";
    MINOR="0";
    PATCH="0";
    ;;
  *)
    # Default, just show the current version
    ;;
esac

echo $MAJOR > .version.major
echo $MINOR > .version.minor
echo $PATCH > .version.patch

echo "${MAJOR}.${MINOR}.${PATCH}" 
